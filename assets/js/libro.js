var Libro = angular.module('libro', ['ngRoute']);

Libro.service('LibroService', function() {
	return {
		store : function(key, obj) {
			return localStorage.setItem(key, JSON.stringify(obj));
		},
		retrieve : function(key) {
			return JSON.parse(localStorage.getItem(key));
		},
		removeUser : function(key) {
			return localStorage.removeItem(key);
		}
	}
});

Libro.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/dashboard', {
		templateUrl : 'dashboard.php',
		controller : 'DashboardController'
	})
	.when('/register', {
		templateUrl : 'register.php',
		controller : 'RegisterController'
	})
	.when('/login',{
		templateUrl : 'login.php',
		controller : 'LoginController',
	})
	.when('/logout', {
		templateUrl : 'logout.php',
		controller : 'LogoutController'
	})
	.when('/add-story', {
		templateUrl : 'add-story.php',
		controller : 'AddStoryController'
	})
	.when('/story/:id', {
		templateUrl : 'story.php',
		controller : 'ViewStoryController'
	})	
	.when('/edit-story/:id', {
		templateUrl : 'edit-story.php',
		controller : 'EditStoryController'
	})
	.when('/', {
		templateUrl : 'home.php',
		controller : 'HomeController',
	})
}]);

Libro.controller('HomeController', ['$scope', '$http', 'LibroService', function($scope, $http, LibroService){
	$scope.stories = [];
	$http.get('server/stories.php').then(function(response) {
		angular.forEach(response.data, function(data) {
			$scope.stories.push(data);
		});
	});
}]);

Libro.controller('RegisterController', ['$scope', '$http', 'LibroService', function($scope, $http, LibroService){

}]);

Libro.controller('LoginController',  ['$scope', '$http', '$location', 'LibroService', function($scope, $http, $location, LibroService){

	$scope.login = function(event) {
		event.preventDefault();

		$http.get('server/checkuser.php?username=' + $scope.username).then(function(response) {
			angular.forEach(response.data, function(data) {
				LibroService.store('user', data);
			});

			if (LibroService.retrieve('user')) {
				$location.path('/dashboard');
			}
		});
	}	
}]);

Libro.controller('DashboardController', ['$scope', '$location', 'LibroService', function($scope, $location, LibroService) {
	if (LibroService.retrieve('user')) {
		$scope.user = {
			name : LibroService.retrieve('user').username
		};
	} else {
		$location.path('/');
	}
}]);

Libro.controller('LogoutController', ['$scope', '$location', 'LibroService', function($scope, $location, LibroService) {
	LibroService.removeUser('user');
	$location.path('/');
}]);

Libro.controller('AddStoryController', ['$scope', '$location', '$http', 'LibroService', function($scope, $location, $http, LibroService){
	if (LibroService.retrieve('user')) {

		$scope.save = function(event) {
			event.preventDefault();
			
			var author_id = LibroService.retrieve('user').id;
			var scenes = [];
			scenes.push($scope.scene_1);

			var data = { author_id : author_id, title : $scope.title, scenes : scenes };

			$http
				.post('save-story.php', data)
				.success(function(data, status, headers, config) {
					if (data.success) {
						$location.path('/');
					} 
				})
				.error(function(data, status) {
					console.log(data);
				});		
		}

	} else {
		$location.path('/');
	}
}]);

Libro.controller('ViewStoryController', ['$scope', '$http', '$routeParams', function($scope, $http , $routeParams){
	$scope.story = [];

	$http.get('server/specificstory.php?' + $routeParams.id).then(function(response) {

	});
}]);
