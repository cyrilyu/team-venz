
<div class="top">
	<a href="#/login"><button>Login</button></a>
	<a href="#/register"><button>Register</button></a>
	<p class="title">Stories</p>	
	<div class="story" ng-repeat="story in stories">
		<p class="author">{{ story.username | uppercase }} </p>
		<p class="story-title">{{ story.title }}</p>
		<p class="content">{{ story.content }}</p>
		<p class="button"><button><a href="#/story/{{ story.id }}">READ STORY</a></button></p>
		<p class="like">Likes : {{ story.like_count }}</p>
	</div>

	<a href="#/add-story">Add Story</a>

</div>