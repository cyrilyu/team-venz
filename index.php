<!DOCTYPE html>
<html ng-app="libro">
<head>
	<meta charset="UTF-8">
	<title>Villas Libro</title>
	<link rel="stylesheet" href="assets/css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
</head>
<body>

	<div class="container">
		<div ng-view></div>
	</div>
		
	<script type="text/javascript" src="assets/js/angular.js"></script>
	<script type="text/javascript" src="assets/js/angular-route.js"></script>
	<script type="text/javascript" src="assets/js/angular-resource.js"></script>
	<script type="text/javascript" src="assets/js/libro.js"></script>
</body>
</html>