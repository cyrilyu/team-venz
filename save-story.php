<?php

require_once dirname(__FILE__).'/server/Story.php';
require_once dirname(__FILE__).'/server/Scene.php';

$data = json_decode(file_get_contents('php://input'), true);

try {
	$story = new Story();
	$story->setAuthorId($data['author_id']);
	$story->setTitle($data['title']);
	$storyId = $story->save();

	foreach ($data['scenes'] as $each) {
		$scene = new Scene();
		$scene->setStoryId($storyId);
		$scene->setUserId($data['author_id']);
		$scene->setLikeCount(0);
		$scene->setContent($each);
		$sceneId = $scene->save();
	}

	$success = true;
	$msg = 'Sucessfully saved story';
} catch (\Exception $e) {
	$success = false;
	$msg = $e->getMessage();
}

echo json_encode(array(
	'msg' 		=> $msg,
	'success'	=> $success
));