<?php

use \PDO as PDO;

class Connection extends PDO
{
	public function __construct()
	{
		parent::__construct('mysql:host=127.0.0.1;dbname=story-book', 'root', '');
	}
}