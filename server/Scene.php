<?php

use \PDO as PDO;

require_once 'Connection.php';

class Scene extends Connection
{
	private $id;

	private $storyId;

	private $userId;

	private $likeCount;

	private $content;

	public function __construct()
	{
		parent::__construct();
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setStoryId($storyId)
	{
		$this->storyId = $storyId;
	}

	public function getStoryId()
	{
		return $this->storyId;
	}

	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	public function getUserId()
	{
		return $this->userId;
	}

	public function setLikeCount($likesCnt)
	{
		$this->likeCount = $likesCnt;
	}

	public function getLikeCount()
	{
		return $this->likeCount;
	}

	public function setContent($content)
	{
		$this->content = $content;
	}

	public function getContent()
	{
		return $this->content;
	}

	public function save()
	{
		if (is_null($this->id)) {
			$sql = "INSERT INTO scenes (`story_id`, `user_id`, `likes`, `content`) VALUES (:story_id, :user_id, :like_count, :content)";
			$stmt = $this->prepare($sql);
			$stmt->bindValue(':story_id', $this->storyId, PDO::PARAM_INT);
			$stmt->bindValue(':user_id', $this->userId, PDO::PARAM_INT);
			$stmt->bindValue(':like_count', $this->likeCount, PDO::PARAM_INT);
			$stmt->bindValue(':content', $this->content, PDO::PARAM_STR);
			$stmt->execute();

			$this->id = $this->lastInsertId();

			return $this->id;
		} else {
			$sql = "UPDATE scenes SET `content` = :content WHERE `id` = :id";
			$stmt = $this->prepare($sql);
			$stmt->bindValue(':content', $this->content, PDO::PARAM_STR);
			$stmt->execute();

			return $this->id;
		}
	}
}