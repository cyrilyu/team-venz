<?php

use \PDO as PDO;

require_once 'Connection.php';

class Story extends Connection
{
	protected $id;

	protected $authorId;

	protected $title;

	public function __construct()
	{
		parent::__construct();
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setAuthorId($authorId)
	{
		$this->authorId = $authorId;
	}

	public function getAuthorId()
	{
		return $this->authorId;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function getAllStories()
	{
		$sql = "SELECT * FROM stories, users, scenes
				WHERE stories.owner_id = users.id
				AND stories.id = scenes.story_id";
		$stmt = $this->prepare($sql);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function save()
	{
		if (is_null($this->id)) {
			$sql = "INSERT INTO stories (`owner_id`, `title`) VALUES (:author_id, :title)";
			$stmt = $this->prepare($sql);
			$stmt->bindValue(':author_id', $this->authorId, PDO::PARAM_INT);
			$stmt->bindValue(':title', $this->title, PDO::PARAM_STR);
			$stmt->execute();

			$this->id = $this->lastInsertId();

			return $this->id;
		} else {
			$sql = "UPDATE stories SET `title` = :title WHERE `id` = :id";
			$stmt = $this->prepare($sql);
			$stmt->bindValue(':title', $this->title, PDO::PARAM_STR);
			$stmt->execute();

			return $this->id;
		}
	}
}