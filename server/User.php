<?php

use \PDO as PDO;

require_once 'Connection.php';

class User extends Connection
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getUser($username)
	{
		$sql = "SELECT * FROM users WHERE users.username = :username";
		$stmt = $this->prepare($sql);
		$stmt->bindValue(':username', $username);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}	
}